function FindProxyForURL(url, host){
  var country = 'IL';
	var myip = myIpAddress();
	var ipbits = myip.split('.');
	var myseg = parseInt(ipbits[3]);
	var p = [3223469900, 3223469902, 3226589467, 628805681, 3223468332];
	for(var i in p){
		n = p[i];
		var d = n%256;
		for(var j = 3; j > 0; j--){
			n = Math.floor(n/256);
			d = n%256 + '.' + d;
		}
		p[i] = d;
	}
	var proxy_configs = [
		'PROXY '+p[0]+':80; PROXY '+p[1]+':80',
		'PROXY '+p[1]+':80; PROXY '+p[0]+':80'
	];
	var netflix_proxy_configs = [
		'PROXY '+p[1]+':80; PROXY '+p[2]+':80',
		'PROXY '+p[2]+':80; PROXY '+p[1]+':80'
	];
	var netflix_proxies = netflix_proxy_configs[myseg % 2];
	var proxies = proxy_configs[myseg % 2];
	if(url == 'https://mediahint.com/mediahint.check.js'){
		return proxies;
	}
	if(country === 'US'){
		proxies = 'DIRECT';
	}
	if(country !== 'GB'){
		var ukproxies = 'PROXY '+p[3]+':80';
	}
	if((host == 'localhost')||(shExpMatch(host, 'localhost.*'))||(shExpMatch(host, '*.local'))||(host == '127.0.0.1')){
		return 'DIRECT';
	}
	if(host == 'ihost.netflix.com'){
		return 'DIRECT';
	}
	if(isPlainHostName(host) ||
		shExpMatch(host, '*.local') ||
		isInNet(dnsResolve(host), '10.0.0.0', '255.0.0.0') ||
		isInNet(dnsResolve(host), '172.16.0.0',  '255.240.0.0') ||
		isInNet(dnsResolve(host), '192.168.0.0',  '255.255.0.0') ||
		isInNet(dnsResolve(host), '127.0.0.0', '255.255.255.0')){
		return 'DIRECT';
	}
	if(shExpMatch(host, '/^\d+\.\d+\.\d+\.\d+$/g')){
		if(isInNet(host, '10.0.0.0', '255.0.0.0')||isInNet(host, '192.168.0.0', '255.255.0.0')) {
			return 'DIRECT';
		}
	}
	if((/(^link\.theplatform\.com$)|(^urs\.pbs\.org$)/).test(host)){
		return 'PROXY '+p[2]+':80';
	}
	if((/(^videocgi\.drt\.cbsig\.net$)|(^media\.cwtv\.com$)/).test(host)){
		return proxies;
	}
	if((/^www\.slacker\.com$/).test(host)&&(/\/(xslte\/userContent)|(wsv1\/session)/).test(url)){
		return proxies;
	}
	if((/^video\.nbcuni\.com$/).test(host)&&(/geo\.xml/).test(url)){
		return proxies;
	}
	if((/songza\.com\/config\.js|geofilter|\/video\/geolocation|geoCountry\.xml|geo-check|\.ism\/manifest|\/services\/viewer\/(htmlFederated|federated_f9)|\/services\/messagebroker\/amf/).test(url)){
		return proxies;
	}
	if((/atv-(ps|ext)\.amazon\.com/).test(host)){
		return proxies;
	}
	if((/oscarapp\/config/).test(url)){
		return proxies;
	}
	if((/^api\.abc\.com$|^w88\.go\.com$/).test(host)){
		return proxies;
	}
	if((/^(www\.)?thewb\.com$/).test(host)){
		return proxies;
	}
	if((/^(www\.|ext\.)?last\.fm$/).test(host)){
		return 'PROXY '+p[2]+':80';
	}
	if(country !== 'GB' && ((/^media\.mtvnservices\.com$/).test(host) || (/^intl\.esperanto\.mtvi\.com$/).test(host)) && (/nick\.co\.uk/).test(url) && (/\.swf/).test(url) === false){
		return ukproxies;
	}
	if(
		(/^media\.mtvnservices\.com$/).test(host)
		||(/www\.spike\.com\/feeds\/mediagen/).test(url)
		||(/\/widgets\/geo\/geoload\.jhtml/).test(url)
		||(/\/includes\/geo\.jhtml/).test(url)
		||(/activity\.flux\.com\/geo\.html/).test(url)
		||(/\/mediaGen\.jhtml/).test(url)
		||(/geocheck\.turner\.tv\.edgesuite\.net/).test(host)
	){
		return proxies;
	}
	if((/^music\.twitter\.com$/).test(host) && ['AU','GB','US','CA','NZ','IE'].indexOf(country) === -1){
		return proxies;
	}
	if((/^video\.query\.yahoo\.com$/).test(host) && (/yahoo\.media\.video\.streams/).test(url)){
		return proxies;
	}
	if((/songza\.com\/(api|advertising)\/|hulu\.com\/mozart\/.*|\.(ico|jpg|png|gif|mp3|js|css|mp4|flv|swf|json)(\?.*)?$|^crackle\.com\/flash\/$/).test(url)||(/(^presentationtracking|blog)\.netflix\.com$|^(r|p|t2|ll\.a|t|t-l3|ads|assets|urlcheck)\.hulu\.com$|^(stats|blog|audio.*|const.*|mediaserver.*|cont.*)\.pandora\.com$/).test(host)){
		return 'DIRECT';
	}
	if((/^([\w\.-]+\.)?hulu\.com$/).test(host)){
		return 'PROXY '+p[4]+':80';
	}
	if((/(^([\w\.-]+\.)?(pandora|songza|www\.iheart|www\.crackle)\.com$)/).test(host)){
		return proxies;
	}
	if((/(^([\w\.-]+\.)?netflix\.com$)/).test(host)){
		return netflix_proxies;
	}
	if((/turntable\.fm/).test(host)){
		return 'PROXY '+p[4]+':80';
	}
	if(['AD','AU','AT','BE','DK','FO','FI','FR','DE','IE','IT','LI','LU','LV','MX','MC','NL','NZ','NO','PL','PT','ES','SE','CH','GB','US','HK','EE','LT','MY','SG','IS'].indexOf(country) === -1){
		if((/^([\w\.-]+\.)?spotify\.com/).test(host)){
			return 'PROXY '+p[2]+':80';
		}
	}
	if((/www\.bbc\.co\.uk\/iplayer\/config\//).test(url)){
		return 'DIRECT';
	}
	if(country !== 'GB'){
		if(
			(/^(www|news)\.bbc\.co\.uk$/).test(host)
			||((/playlists\.bbc\.co\.uk/).test(host)&&(/\.sxml/).test(url))
			||(/bbc\.co\.uk\/iplayer/).test(url)
			||(/open\.live\.bbc\.co\.uk/).test(host)
			||(/bbc\.co\.uk\.edgesuite\.net\/crossdomain\.xml/).test(url)
			||(/bbcfmhds\.vo\.llnwd\.net\/crossdomain\.xml/).test(url)
			||((/bbcfmhds\.vo\.llnwd\.net|bbc\.co\.uk\.edgesuite\.net/).test(host)&&(/\.(f4m|bootstrap)/).test(url))
			||(/bbc\.co\.uk\.edgesuite\.net\/hds-live\/streams\/livepkgr\/streams/).test(url)
			||(/bbc\.co\.uk\/mediaselector/).test(url)
			||(/ais\.channel4\.com/).test(host)
			||(/mercury\.itv\.com/).test(host)
			||(/bbc\.co\.uk\/mobile\/apps\/iplayer/).test(url)
			||(/itv\.com\/ukonly/).test(url)
		){
			return ukproxies;
		}
	}
	return 'DIRECT';
}
